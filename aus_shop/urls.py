
from django.conf.urls import include, url
from django.contrib import admin
from oscar.app import application
from paypal.express.dashboard import app
from paypal.payflow.dashboard.app import application as payflow

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^checkout/paypal/', include('paypal.express.urls')),
    # Dashboard views for Payflow Pro
    url(r'^dashboard/paypal/payflow/', include(payflow.urls)),
    url(r'^dashboard/paypal/express/', include(app.application.urls)),
    url(r'', include(application.urls)),
]

