from oscar.apps.partner import strategy, prices
from oscar.apps.order.models import  ShippingAddress
from oscar.apps.address.models import UserAddress
from decimal import Decimal as D
from django.utils.translation import ugettext_lazy as _

class Selector(object):

    def strategy(self, request=None, user=None, **kwargs):
        return Australia_Taxes_Strategy(user,request)


class Australia_Taxes_Strategy(strategy.UseFirstStockRecord, strategy.StockRequired, strategy.FixedRateTax,
                               strategy.Structured):
    rate = D('0.20')
    exponent = D('0.01')

    def __init__(self, user, request=None):
        self.request = request
        self.user = user
        if request and request.user.is_authenticated():
            self.user = request.user

    def get_rate(self, product, stockrecord):

        AUSTRALIAN_GST_TAX = D('0.07')
        return AUSTRALIAN_GST_TAX

        #fol all counties except Australia - taxes = 0
        #GST tax
        #replaced this code for checkout process

        # if self.user:
        #     shipping_address = self.user.addresses.all()
        #     if shipping_address.count() > 0:
        #         if shipping_address[0].country.printable_name != "Australia":
        #             return D('0.00')
        """
        For example, add a strategy, that vary depending from size of painting
        """
        # try:
        #     if product.attr.size.option == _("small paintings"):
        #         return D('0.15')
        #     elif product.attr.size.option == _("medium paintings"):
        #         return D('0.10')
        #     else:
        #         return D('0.05')
        # except:
        #     #if size option accidentally will be removed by administrator
        #     raise AttributeError(
        #     _("%(obj)s has no attribute named '%(attr)s'") % {
        #         'obj': product.get_product_class(), 'attr': 'size'})
        # return self.rate

         #customize view to recaclulate the tax for non-australian customers
        # shipping_address = self.get_shipping_address(self.request.basket)
        #
        # if shipping_address:
        #     if shipping_address.country.printable_name != "Australia":
        #         rate = D('0.00')
        #     else:
        #         rate = AUSTRALIAN_GST_TAX
        #     return rate



    #this metod is redudandant, because of basket instance is updated between requests and best metod for solving
    #task (update basket lines - taxes, basing on shipping address information)  - use CheckoutSessionMixin.build_submission()
    def get_shipping_address(self, basket):

        if not basket.is_shipping_required():
            return None

        SESSION_KEY = 'checkout_data'

        if SESSION_KEY not in self.request.session:
           return None

        try:
            addr_data = self.request.session[SESSION_KEY]['shipping']['new_address_fields']
            if addr_data:
                # Load address data into a blank shipping address model
                return ShippingAddress(**addr_data)
        except:
            pass


        try:
            addr_id =   self.request.session[SESSION_KEY]['shipping']['user_address_id']
            if addr_id:
                try:
                    address = UserAddress._default_manager.get(pk=addr_id)
                except UserAddress.DoesNotExist:
                    return None
                else:
                    shipping_addr = ShippingAddress()
                    address.populate_alternative_model(shipping_addr)
                    return shipping_addr
        except:
            return None


    def get_exponent(self, stockrecord):
        """
        This method serves as hook to be able to plug in support for a varying exponent
        based on the currency.

        TODO: Needs tests.
        """
        return self.exponent