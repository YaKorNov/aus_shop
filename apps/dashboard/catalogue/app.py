from django.conf.urls import url, include
from oscar.apps.dashboard.catalogue.app import CatalogueApplication as CoreDashboardCatalogueApplication
from .views import GroupOptionsListView, GroupOptionsCreateView, GroupOptionsUpdateView, GroupOptionsDeleteView,\
    ArtistsListView, ArtistsCreateView, ArtistsUpdateView, ArtistsDeleteView
    # ProductClassUpdateView
# from oscar.core.application import Application
# from oscar.core.loading import get_class


class CatalogueApplication(CoreDashboardCatalogueApplication):
    group_options_list_view = GroupOptionsListView
    group_options_create_view = GroupOptionsCreateView
    group_options_update_view = GroupOptionsUpdateView
    group_options_delete_view = GroupOptionsDeleteView
    artists_list_view = ArtistsListView
    artists_create_view = ArtistsCreateView
    artists_update_view = ArtistsUpdateView
    artists_delete_view = ArtistsDeleteView

    def get_urls(self):
        urls = [
            url(r'^products/(?P<pk>\d+)/$',
                self.product_createupdate_view.as_view(),
                name='catalogue-product'),
            url(r'^products/create/$',
                self.product_create_redirect_view.as_view(),
                name='catalogue-product-create'),
            url(r'^products/create/(?P<product_class_slug>[\w-]+)/$',
                self.product_createupdate_view.as_view(),
                name='catalogue-product-create'),
            url(r'^products/(?P<parent_pk>[-\d]+)/create-variant/$',
                self.product_createupdate_view.as_view(),
                name='catalogue-product-create-child'),
            url(r'^products/(?P<pk>\d+)/delete/$',
                self.product_delete_view.as_view(),
                name='catalogue-product-delete'),
            url(r'^$', self.product_list_view.as_view(),
                name='catalogue-product-list'),
            url(r'^stock-alerts/$', self.stock_alert_view.as_view(),
                name='stock-alert-list'),
            url(r'^product-lookup/$', self.product_lookup_view.as_view(),
                name='catalogue-product-lookup'),
            url(r'^categories/$', self.category_list_view.as_view(),
                name='catalogue-category-list'),
            url(r'^categories/(?P<pk>\d+)/$',
                self.category_detail_list_view.as_view(),
                name='catalogue-category-detail-list'),
            url(r'^categories/create/$', self.category_create_view.as_view(),
                name='catalogue-category-create'),
            url(r'^categories/create/(?P<parent>\d+)$',
                self.category_create_view.as_view(),
                name='catalogue-category-create-child'),
            url(r'^categories/(?P<pk>\d+)/update/$',
                self.category_update_view.as_view(),
                name='catalogue-category-update'),
            url(r'^categories/(?P<pk>\d+)/delete/$',
                self.category_delete_view.as_view(),
                name='catalogue-category-delete'),
            url(r'^product-type/create/$',
                self.product_class_create_view.as_view(),
                name='catalogue-class-create'),
            url(r'^product-types/$',
                self.product_class_list_view.as_view(),
                name='catalogue-class-list'),
            url(r'^product-type/(?P<pk>\d+)/update/$',
                self.product_class_update_view.as_view(),
                name='catalogue-class-update'),
            url(r'^product-type/(?P<pk>\d+)/delete/$',
                self.product_class_delete_view.as_view(),
                name='catalogue-class-delete'),
            #here we add CRUD urls for groyp options
            url(r'^group-options-list/', self.group_options_list_view.as_view(),
                name='catalogue-group-options-attributes-list'),
            url(r'^group-options-create/', self.group_options_create_view.as_view(),
                name='catalogue-group-options-create'),
            url(r'^group-options/(?P<pk>\d+)/$',
                self.group_options_update_view.as_view(),
                name='catalogue-group-options-attribute'),
            url(r'^group-options/(?P<pk>\d+)/delete/$',
                self.group_options_delete_view.as_view(),
                name='catalogue-group-options-attribute-delete'),
            #here we add CRUD urls for artists
            url(r'^artists-list/', self.artists_list_view.as_view(),
                name='catalogue-artists-list'),
            url(r'^artists-create/', self.artists_create_view.as_view(),
                name='catalogue-artists-create'),
            url(r'^artists/(?P<pk>\d+)/update/$',
                self.artists_update_view.as_view(),
                name='catalogue-artists-update'),
            url(r'^artists/(?P<pk>\d+)/delete/$',
                self.artists_delete_view.as_view(),
                name='catalogue-artists-delete'),
        ]
        return self.post_process_urls(urls)

application = CatalogueApplication()