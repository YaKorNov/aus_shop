from django import forms
from django.core import exceptions
from django.forms.models import inlineformset_factory
from django.utils.translation import ugettext_lazy as _
from treebeard.forms import movenodeform_factory

from oscar.core.loading import get_class, get_model
from oscar.core.utils import slugify
from oscar.apps.dashboard.catalogue.forms import ProductForm as BaseProductForm
from oscar.forms.widgets import ImageInput
from django.forms.models import BaseInlineFormSet
from django.core.exceptions import ValidationError


AttributeOptionGroup = get_model('catalogue', 'AttributeOptionGroup')
AttributeOption = get_model('catalogue', 'AttributeOption')
Product = get_model('catalogue', 'Product')
ProductClass = get_model('catalogue', 'ProductClass')
ProductAttribute = get_model('catalogue', 'ProductAttribute')
Artist = get_model('catalogue', 'Artist')

class AttributeOptionGroupForm(forms.ModelForm):

    class Meta:
        model = AttributeOptionGroup
        fields = ('name', )




class AttributeOptionsForm(forms.ModelForm):

    class Meta:
        model = AttributeOption
        fields = ["option",]

AttributeOptionsFormSet = inlineformset_factory(AttributeOptionGroup,
                                                 AttributeOption,
                                                 form=AttributeOptionsForm,
                                                 extra=3)


class ProductAttributesForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ProductAttributesForm, self).__init__(*args, **kwargs)

        # because we'll allow submission of the form with blank
        # codes so that we can generate them.
        self.fields["code"].required = False

        self.fields["option_group"].help_text = _("Select an option group")

    def clean_code(self):
        code = self.cleaned_data.get("code")
        title = self.cleaned_data.get("name")

        if not code and title:
            code = slugify(title)

        return code

    class Meta:
        model = ProductAttribute
        fields = ["name", "code", "type", "option_group", "required"]




class ProductAttributesInlineFormSet(BaseInlineFormSet):
    def clean(self):
        instances = self.save(commit=False)
        for attr_obj in self.deleted_objects:
            # your custom formset validation
            if attr_obj.productattributevalue_set.count() > 0:
                raise ValidationError(_('You have some products with attribute %(value)s installed'),
                                      params={'value': attr_obj.name},)
        super(ProductAttributesInlineFormSet, self).clean()




ProductAttributesFormSet = inlineformset_factory(ProductClass,
                                                 ProductAttribute,
                                                 form=ProductAttributesForm,
                                                 formset=ProductAttributesInlineFormSet,
                                                 extra=3)


class ArtistForm(forms.ModelForm):

    class Meta:
        model = Artist
        fields = ('first_name', 'surname', 'last_name', 'slug', 'description',
                  'photo', )



def _attr_text_field(attribute):
    return forms.CharField(label=attribute.name,
                           required=attribute.required)


def _attr_textarea_field(attribute):
    return forms.CharField(label=attribute.name,
                           widget=forms.Textarea(),
                           required=attribute.required)


def _attr_integer_field(attribute):
    return forms.IntegerField(label=attribute.name,
                              required=attribute.required)


def _attr_boolean_field(attribute):
    return forms.BooleanField(label=attribute.name,
                              required=attribute.required)


def _attr_float_field(attribute):
    return forms.FloatField(label=attribute.name,
                            required=attribute.required)


def _attr_date_field(attribute):
    return forms.DateField(label=attribute.name,
                           required=attribute.required,
                           widget=forms.widgets.DateInput)


def _attr_option_field(attribute):
    return forms.ModelChoiceField(
        label=attribute.name,
        required=attribute.required,
        queryset=attribute.option_group.options.all())


def _attr_multi_option_field(attribute):
    return forms.ModelMultipleChoiceField(
        label=attribute.name,
        required=attribute.required,
        queryset=attribute.option_group.options.all())


def _attr_entity_field(attribute):
    # Product entities don't have out-of-the-box supported in the ProductForm.
    # There is no ModelChoiceField for generic foreign keys, and there's no
    # good default behaviour anyway; offering a choice of *all* model instances
    # is hardly useful.
    if attribute.code == 'artist':
        return forms.ModelChoiceField(
            label=attribute.name,
            required=attribute.required,
            queryset=Artist.objects.all())
    else:
        return None


def _attr_numeric_field(attribute):
    return forms.FloatField(label=attribute.name,
                            required=attribute.required)


def _attr_file_field(attribute):
    return forms.FileField(
        label=attribute.name, required=attribute.required)


def _attr_image_field(attribute):
    return forms.ImageField(
        label=attribute.name, required=attribute.required)


class ProductForm(BaseProductForm):
    FIELD_FACTORIES = {
        "text": _attr_text_field,
        "richtext": _attr_textarea_field,
        "integer": _attr_integer_field,
        "boolean": _attr_boolean_field,
        "float": _attr_float_field,
        "date": _attr_date_field,
        "option": _attr_option_field,
        "multi_option": _attr_multi_option_field,
        "entity": _attr_entity_field,
        "numeric": _attr_numeric_field,
        "file": _attr_file_field,
        "image": _attr_image_field,
    }
