# -*- coding: utf-8 -*-
from django.views.generic.detail import SingleObjectMixin
from django.conf import settings
from django.template.response import TemplateResponse
from django.contrib import auth, messages
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView, FormView, TemplateView, \
    CreateView, UpdateView, DeleteView
# from oscar.apps.catalogue.models import AttributeOptionGroup, AttributeOption
from django_tables2 import SingleTableMixin
from tables import AttributeOptionGroupTable, ArtistTable
from oscar.core.loading import get_classes, get_model
from forms import AttributeOptionGroupForm, AttributeOptionsFormSet, ArtistForm
from django.contrib.auth import update_session_auth_hash
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from oscar.apps.dashboard.catalogue.views import ProductClassUpdateView as BaseProductClassUpdateView
from forms import ProductAttributesFormSet
import datetime
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
import json


# AttributeOptionGroupTable = get_classes('dashboard.catalogue.tables',
#                   ('ProductTable', 'CategoryTable'))
AttributeOptionGroup = get_model('catalogue', 'AttributeOptionGroup')
AttributeOption = get_model('catalogue', 'AttributeOption')
Artist = get_model('catalogue', 'Artist')



class GroupOptionsListView(SingleTableMixin, TemplateView):

    # model = AttributeOptionGroup
    # context_object_name = 'options_group'
    template_name = 'dashboard/catalogue/list_attribute_option_group.html'
    table_class = AttributeOptionGroupTable
    context_table_name = 'attribute_option_groups'

    def get_queryset(self):
        return AttributeOptionGroup.objects.all()

    # def get_context_data(self, *args, **kwargs):
    #     ctx = super(GroupOptionsListView, self).get_context_data(*args, **kwargs)
    #     ctx['child_categories'] = Category.get_root_nodes()
    #     return ctx




class GroupOptionsCreateUpdateView(UpdateView):

    template_name = 'dashboard/catalogue/attribute_option_group_form.html'
    model = AttributeOptionGroup
    form_class = AttributeOptionGroupForm
    attribute_options_formset = AttributeOptionsFormSet
    # success_url = "dashboard:catalogue-group-options-attributes-list"

    def process_all_forms(self, form):
        """
        This validates both the ProductClass form and the
        ProductClassAttributes formset at once
        making it possible to display all their errors at once.
        """
        if self.creating and form.is_valid():
            # the object will be needed by the product_attributes_formset
            self.object = form.save(commit=False)

        attribute_options_formset = self.attribute_options_formset(
            self.request.POST, self.request.FILES, instance=self.object)

        is_valid = form.is_valid() and attribute_options_formset.is_valid()

        if is_valid:
            return self.forms_valid(form, attribute_options_formset)
        else:
            return self.forms_invalid(form, attribute_options_formset)

    def forms_valid(self, form, attribute_options_formset):
        form.save()
        attribute_options_formset.save()

        return HttpResponseRedirect(self.get_success_url())

    def forms_invalid(self, form, attribute_options_formset):
        messages.error(self.request,
                       _("Your submitted data was not valid - please "
                         "correct the errors below"
                         ))
        ctx = self.get_context_data(form=form,
                                    attribute_options_formset=attribute_options_formset)
        return self.render_to_response(ctx)

    form_valid = form_invalid = process_all_forms

    def get_context_data(self, *args, **kwargs):
        ctx = super(GroupOptionsCreateUpdateView, self).get_context_data(
            *args, **kwargs)

        if "attribute_options_formset" not in ctx:
            ctx["attribute_options_formset"] = self.attribute_options_formset(
                instance=self.object)

        ctx["title"] = self.get_title()

        return ctx

    # def get_success_url(self):
    #     return reverse("dashboard:catalogue-group-options-attributes-list",)




class GroupOptionsCreateView(GroupOptionsCreateUpdateView):

    creating = True

    def get_object(self):
        return None

    def get_title(self):
        return _("Add a new options group")

    def get_success_url(self):
        messages.info(self.request, _("Options group created successfully"))
        return reverse("dashboard:catalogue-group-options-attributes-list")


class GroupOptionsUpdateView(GroupOptionsCreateUpdateView):

    creating = False

    def get_title(self):
        return _("Update options group '%s'") % self.object.name

    def get_success_url(self):
        messages.info(self.request, _("Options group updated successfully"))
        return reverse("dashboard:catalogue-group-options-attributes-list")

    def get_object(self):
        option_group = get_object_or_404(AttributeOptionGroup, pk=self.kwargs['pk'])
        return option_group




class GroupOptionsDeleteView(DeleteView):
    template_name = 'dashboard/catalogue/attribute_option_group_delete.html'
    model = AttributeOptionGroup
    form_class = AttributeOptionGroupForm

    def get_context_data(self, *args, **kwargs):
        ctx = super(GroupOptionsDeleteView, self).get_context_data(*args,
                                                                   **kwargs)
        ctx['title'] = _("Delete group of attribute options '%s'") % self.object.name
        attributes_count = self.object.productattribute_set.count()

        if attributes_count > 0:
            ctx['disallow'] = True
            ctx['title'] = _("Unable to delete '%s'") % self.object.name
            messages.error(self.request,
                           _("%i product attributes are still assigned to this group") %
                           attributes_count)
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("Group of attribute options deleted successfully"))
        return reverse("dashboard:catalogue-group-options-attributes-list")




# class ProductClassUpdateView(BaseProductClassUpdateView):
#
#     product_attributes_formset = ProductAttributesFormSet

class ArtistsListView(SingleTableMixin, TemplateView):

    template_name = 'dashboard/catalogue/artist_list.html'
    table_class = ArtistTable
    context_table_name = 'artists'

    def get_table_pagination(self):
        return dict(per_page=20)

    def get_queryset(self):
        """
        Build the queryset for this list
        """
        return Artist.objects.all()




class ArtistsCreateView(CreateView):
    template_name = 'dashboard/catalogue/artist_form.html'
    model = Artist
    form_class = ArtistForm

    def get_context_data(self, **kwargs):
        ctx = super(ArtistsCreateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Add a new artist")
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("Artist created successfully"))
        return reverse_lazy('dashboard:catalogue-artists-list')




class ArtistsUpdateView(UpdateView):
    template_name = 'dashboard/catalogue/artist_form.html'
    model = Artist
    form_class = ArtistForm

    def get_context_data(self, **kwargs):
        ctx = super(ArtistsUpdateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Update '%s %s'") % (self.object.first_name, self.object.surname)
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("Artist updated successfully"))
        return reverse_lazy('dashboard:catalogue-artists-list')


class ArtistsDeleteView(DeleteView):
    template_name = 'dashboard/catalogue/artist_delete.html'
    model = Artist

    def get_context_data(self, *args, **kwargs):
        ctx = super(ArtistsDeleteView, self).get_context_data(*args,
                                                                   **kwargs)
        ctx['title'] = _("Delete artist information '%s'") % self.object.first_name
        #TODO make raw SQL query to check that artist have no paintings
        # paintings_count = self.object.productattributevalue_set.count()
        #
        # if paintings_count > 0:
        #     ctx['disallow'] = True
        #     ctx['title'] = _("Unable to delete '%s'") % self.object.first_name
        #     messages.error(self.request,
        #                    _("%i paintings are still assigned to this artist") %
        #                    paintings_count)
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("Artist deleted successfully"))
        return reverse_lazy('dashboard:catalogue-artists-list')