from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _, ungettext_lazy

from django_tables2 import Column, LinkColumn, TemplateColumn, A

from oscar.core.loading import get_class, get_model

DashboardTable = get_class('dashboard.tables', 'DashboardTable')
AttributeOptionGroup = get_model('catalogue', 'AttributeOptionGroup')
AttributeOption = get_model('catalogue', 'AttributeOption')
Artist = get_model('catalogue', 'Artist')


class AttributeOptionGroupTable(DashboardTable):
    name = LinkColumn('dashboard:catalogue-group-options-attribute', args=[A('pk')])
    # title = TemplateColumn(
    #     verbose_name=_('Title'),
    #     template_name='dashboard/catalogue/attribute_option_group_row_title.html',
    #     order_by='title', accessor=A('name'))
    actions = TemplateColumn(
        verbose_name=_('Actions'),
        template_name='dashboard/catalogue/attribute_option_group_row_actions.html',
        orderable=False)

    icon = "sitemap"

    class Meta(DashboardTable.Meta):
        model = AttributeOptionGroup
        fields = ('name', )


# class AttributeOptionTable(DashboardTable):

class ArtistTable(DashboardTable):
    first_name = TemplateColumn(
        verbose_name=_('First name'),
        accessor=A('first_name'),
        order_by='first_name',
        template_name='dashboard/catalogue/artist_row_title.html',)
    surname = Column(
        verbose_name=_('Surname'),
        accessor=A('surname'),
        order_by='surname')
    lastname = Column(
        verbose_name=_('Last_name'),
        accessor=A('last_name'),
        order_by='last_name')
    image = TemplateColumn(
        verbose_name=_('Photo'),
        template_name='dashboard/catalogue/artist_row_image.html',
        orderable=False)
    actions = TemplateColumn(
        verbose_name=_('Actions'),
        template_name='dashboard/catalogue/artist_actions.html',
        orderable=False)

    icon = "sitemap"

    class Meta(DashboardTable.Meta):
        model = Artist
        fields = ('date_updated', )
        sequence = ('image', 'first_name', 'surname',
                    'lastname', '...', 'date_updated', 'actions')
        order_by = '-date_updated'