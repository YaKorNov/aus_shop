from decimal import Decimal as D

from django.utils.translation import ugettext_lazy as _
from oscar.apps.shipping import methods
from oscar.core import prices



class Australia_Post(methods.Free):
    """
    This shipping method specifies that shipping is free.
    """
    code = 'australia-post'
    name = _('Australia Post')

class Australian_Air_Express(methods.Free):
    """
    This shipping method specifies that shipping is free.
    """
    code = 'australian-air-express'
    name = _('Australian Air Express (StarTrack)')

class TNT_Courier(methods.Free):
    """
    This shipping method specifies that shipping is free.
    """
    code = 'tnt-Courier'
    name = _('TNT Courier')

class Australia_Post_Air_Mail(methods.Free):
    """
    This shipping method specifies that shipping is free.
    """
    code = 'australia-post-air-mail '
    name = _('Australia Post Air Mail ')

class International_Courier(methods.Free):
    """
    This shipping method specifies that shipping is free.
    """
    code = 'international-courier'
    name = _('International Courier')


