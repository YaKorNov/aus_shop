from decimal import Decimal as D

from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _

from oscar.apps.shipping import repository
from methods import Australia_Post, Australian_Air_Express, TNT_Courier, Australia_Post_Air_Mail, International_Courier


class Repository(repository.Repository):

    def get_available_shipping_methods(
            self, basket, shipping_addr=None, **kwargs):

        available_methods = []
        products_in_basket = list([line.product for line in basket.lines.all() ])

        if shipping_addr and shipping_addr.country.printable_name == "Australia":

            for product in products_in_basket:
                try:
                    if product.attr.size.option == _('small paintings'):
                        available_methods.append(Australia_Post)
                    elif product.attr.size.option == _('large paintings'):
                        available_methods.append(TNT_Courier)
                    else:
                        available_methods.append(Australian_Air_Express)
                except:
                    #if size option accidentally will be removed by administrator
                    raise AttributeError(
                        _("%(obj)s has no attribute named '%(attr)s'") % {
                            'obj': product.get_product_class(), 'attr': 'size'})
        elif shipping_addr and shipping_addr.country.printable_name != "Australia":

            for product in products_in_basket:
                try:
                    if product.attr.size.option == _('large paintings'):
                        available_methods.append(International_Courier)
                    else:
                        available_methods.append(Australia_Post_Air_Mail)
                except:
                    #if size option accidentally will be removed by administrator
                    raise AttributeError(
                        _("%(obj)s has no attribute named '%(attr)s'") % {
                            'obj': product.get_product_class(), 'attr': 'size'})
        else:
            available_methods = [Australia_Post, Australian_Air_Express, TNT_Courier, Australia_Post_Air_Mail,
                                 International_Courier]

        return tuple([m() for m in set(available_methods)])

        # return self.methods

