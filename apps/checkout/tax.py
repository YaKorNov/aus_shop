from decimal import Decimal as D

def apply_to(shipping_address,basket):

    AUSTRALIAN_GST_TAX = D('0.07')

    if shipping_address.country.printable_name != "Australia":
        rate = D('0.00')
    else:
        rate = AUSTRALIAN_GST_TAX

    for line in basket.all_lines():
        line_tax = calculate_tax(
            line.line_price_excl_tax_incl_discounts, rate)
        unit_tax = (line_tax / line.quantity).quantize(D('0.01'))
        line.purchase_info.price.tax = unit_tax

    # Because of shipping metods is all free - don't recalculate

    # shipping_method = submission['shipping_method']
    # shipping_method.tax = calculate_tax(
    #     shipping_method.charge_incl_tax, rate)

def calculate_tax(price, rate):
        tax = price * rate
        return tax.quantize(D('0.01'))