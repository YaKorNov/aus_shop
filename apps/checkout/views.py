import logging

from django import http
from django.shortcuts import redirect
from django.contrib import messages
from django.core.urlresolvers import reverse, reverse_lazy
from django.utils.http import urlquote
from django.utils.translation import ugettext as _
from django.utils import six
from django.views import generic

from oscar.apps.checkout import views, session
from oscar.apps.payment import models
from oscar.apps.payment.forms import BankcardForm
from oscar.apps.payment.models import SourceType, Source
from oscar.apps.address.models import UserAddress

from datacash.facade import Facade
from django.conf import settings
from datacash import the3rdman
from paypal.payflow import facade as paypal_facade
from . import tax

# ==============
# Payment method
# ==============


class PaymentMethodView(session.CheckoutSessionMixin, generic.TemplateView):

    template_name = 'checkout/payment_methods.html'
    pre_conditions = [
        'check_basket_is_not_empty',
        'check_basket_is_valid',
        'check_user_email_is_captured',
        'check_shipping_data_is_captured']
    skip_conditions = ['skip_unless_payment_is_required']

    def get(self, request, *args, **kwargs):

        self._methods = self.get_available_payment_methods()

        if len(self._methods) == 0:
            messages.warning(request, _(
                "There are not available shipping methods"))
            return redirect('checkout:shipping-method')
        elif len(self._methods) == 1:
            self.checkout_session.pay_by(self._methods[0])
            return self.get_success_response()

        return super(PaymentMethodView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs = super(PaymentMethodView, self).get_context_data(**kwargs)
        kwargs['methods'] = self._methods
        return kwargs

    def get_available_payment_methods(self):
        return ('PayPal', 'BankCard', )

    def is_valid_payment_method(self, method_code):
        for method in self.get_available_payment_methods():
            if method == method_code:
                return True
        return False

    def post(self, request, *args, **kwargs):
        # Need to check that this code is valid for this user
        method_code = request.POST.get('method_code', None)
        if not self.is_valid_payment_method(method_code):
            messages.error(request, _("Your submitted payment method is not"
                                      " permitted"))
            return redirect('checkout:payment-method')

        self.checkout_session.pay_by(method_code)

        return self.get_success_response()

    def get_success_response(self):
        return redirect('checkout:payment-details')

# ================
# Order submission
# ================

#TODO need to refactor code
class PaymentDetailsView(views.PaymentDetailsView):

    def get_context_data(self, **kwargs):
        # Add bankcard form to the template context
        ctx = super(PaymentDetailsView, self).get_context_data(**kwargs)
        ctx['bankcard_form'] = kwargs.get('bankcard_form', BankcardForm())
        ctx['pay_pal'] = (self.checkout_session.payment_method() == 'PayPal')
        return ctx

    def handle_payment_details_submission(self, request):

        if self.checkout_session.payment_method() == 'PayPal':
           return super(PaymentDetailsView, self).handle_payment_details_submission(request)

        # Check bankcard form is valid
        bankcard_form = BankcardForm(request.POST)
        if bankcard_form.is_valid():

            #update busket lines for tax amount
            # shipping_address = self.get_shipping_address(self.request.basket)
            # tax.apply_to(shipping_address,self.request.basket)

            return self.render_preview(
                request, bankcard_form=bankcard_form)

        # Form invalid - re-render
        return self.render_payment_details(
            request, bankcard_form=bankcard_form)

    def handle_place_order_submission(self, request):

        if self.checkout_session.payment_method() == 'PayPal':
           return super(PaymentDetailsView, self).handle_place_order_submission(request)

        bankcard_form = BankcardForm(request.POST)
        if bankcard_form.is_valid():
            submission = self.build_submission(
                payment_kwargs={
                    'bankcard_form': bankcard_form
                })
            return self.submit(**submission)

        messages.error(request, _("Invalid submission"))
        return http.HttpResponseRedirect(
            reverse('checkout:payment-details'))

    def build_submission(self, **kwargs):
        # Ensure the shipping address is part of the payment keyword args
        submission = super(PaymentDetailsView, self).build_submission(**kwargs)

        if self.checkout_session.payment_method() == 'PayPal':
           return submission

        submission['payment_kwargs']['shipping_address'] = submission[
            'shipping_address']
        return submission

    def post(self, request, *args, **kwargs):
        # Override so we can validate the bankcard/billingaddress submission.
        # If it is valid, we render the preview screen with the forms hidden
        # within it.  When the preview is submitted, we pick up the 'action'
        # parameters and actually place the order.

        if self.checkout_session.payment_method() == 'BankCard':
           return super(PaymentDetailsView, self).post(request, *args, **kwargs)

        if request.POST.get('action', '') == 'place_order':
            return self.do_place_order(request)

        bankcard_form = BankcardForm(request.POST)
        # billing_address_form = forms.BillingAddressForm(request.POST)
        if not bankcard_form.is_valid():
            # Form validation failed, render page again with errors
            self.preview = False
            ctx = self.get_context_data(
                bankcard_form=bankcard_form)
            return self.render_to_response(ctx)

        #update busket lines for tax amount
        # shipping_address = self.get_shipping_address(self.request.basket)
        # tax.apply_to(shipping_address,self.request.basket)

        # Render preview with bankcard and billing address details hidden
        return self.render_preview(request,
                                   bankcard_form=bankcard_form)

    def do_place_order(self, request):
        # Helper method to check that the hidden forms wasn't tinkered
        # with.
        bankcard_form = BankcardForm(request.POST)
        if not bankcard_form.is_valid():
            messages.error(request, "Invalid submission")
            return http.HttpResponseRedirect(reverse('checkout:payment-details'))

        # Attempt to submit the order, passing the bankcard object so that it
        # gets passed back to the 'handle_payment' method below.
        submission = self.build_submission()
        submission['payment_kwargs']['bankcard'] = bankcard_form.bankcard
        return self.submit(**submission)

    def handle_payment(self, order_number, total, **kwargs):
        # pass
        if self.checkout_session.payment_method() == 'BankCard':
            # Make request to DataCash - if there any problems (eg bankcard
            # not valid / request refused by bank) then an exception would be
            # raised and handled)
            facade = Facade()

            # Use The3rdMan - so build a dict of data to pass
            # email = None
            # if not self.request.user.is_authenticated():
            #     email = self.checkout_session.get_guest_email()
            # fraud_data = the3rdman.build_data_dict(
            #     request=self.request,
            #     email=email,
            #     order_number=order_number,
            #     shipping_address=kwargs['shipping_address'])

            # We're not using 3rd-man by default
            bankcard = kwargs['bankcard_form'].bankcard

            # commented this lines only for develompent goals
            # datacash_ref = facade.authorise(
            #     order_number, total.incl_tax, bankcard)

            #after successful money operation
            for line in self.request.basket.all_lines():
                # we allocate the order lines on checkout
                line.stockrecord.allocate(line.quantity)


            source_type, _ = SourceType.objects.get_or_create(name='Datacash')
            source = Source(source_type=source_type,
                            currency=settings.DATACASH_CURRENCY,
                            amount_debited=total.incl_tax,
                            reference='from card')
            self.add_payment_source(source)
             # Also record payment event
            self.add_payment_event('Auth', total.incl_tax)

        else:
             # Using authorization here (two-stage model).  You could use sale to
            # perform the auth and capture in one step.  The choice is dependent
            # on your business model.
            paypal_facade.sale(
                order_number, total.incl_tax,
                kwargs['bankcard'])

            # Record payment source and event
            source_type, is_created = models.SourceType.objects.get_or_create(
                name='PayPal')
            source = source_type.sources.model(
                source_type=source_type,
                amount_allocated=total.incl_tax, currency=total.currency)
            self.add_payment_source(source)
            self.add_payment_event('Sale', total.incl_tax)

