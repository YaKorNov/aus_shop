from oscar.apps.checkout.session import CheckoutSessionMixin as BaseCheckoutSessionMixin
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from oscar.core.loading import get_model, get_class
from oscar.apps.checkout import exceptions
from decimal import Decimal as D
from oscar.apps.checkout import session

from . import tax

class CheckoutSessionMixin(BaseCheckoutSessionMixin):

    def check_payment_data_is_captured(self, request):

        if request.method == 'POST':
            pass
        elif request.method == 'GET':
            raise exceptions.FailedPreCondition(
                url=reverse('checkout:payment-details'),
                message=_("You must fill the form with nesessary data ")
            )

        pass

    def build_submission(self, **kwargs):

        submission = super(CheckoutSessionMixin, self).build_submission(
            **kwargs)

        if submission['shipping_address'] and submission['shipping_method']:
            tax.apply_to(submission['shipping_address'],submission['basket'])

            # Recalculate order total to ensure we have a tax-inclusive total

            shipping_charge = submission['shipping_method'].calculate(submission['basket'])

            submission['order_total'] = self.get_order_totals(
                submission['basket'],
                shipping_charge)

        return submission


