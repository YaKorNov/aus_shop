# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0006_auto_20150911_1835'),
    ]

    operations = [
        migrations.CreateModel(
            name='Artist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=255, verbose_name='First name')),
                ('surname', models.CharField(max_length=255, verbose_name='Surname')),
                ('last_name', models.CharField(max_length=255, verbose_name='Last name', blank=True)),
                ('slug', models.SlugField(unique=True, max_length=255, verbose_name='Slug')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('photo', models.ImageField(max_length=255, upload_to=b'artists_photos', null=True, verbose_name='Photo', blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Date created')),
                ('date_updated', models.DateTimeField(auto_now=True, verbose_name='Date updated', db_index=True)),
            ],
            options={
                'ordering': ['-first_name'],
                'verbose_name': 'Artist',
                'verbose_name_plural': 'Artists',
            },
        ),
    ]
