from oscar.apps.catalogue.models import *  # noqa

from django.core.urlresolvers import reverse
from django.core.validators import RegexValidator
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _, pgettext_lazy
from oscar.models.fields import NullCharField, AutoSlugField

@python_2_unicode_compatible
class Artist(models.Model):

    first_name = models.CharField(_('First name'),
                             max_length=255, blank=False)
    surname = models.CharField(_('Surname'),
                             max_length=255, blank=False)
    last_name = models.CharField(_('Last name'),
                             max_length=255, blank=True)
    slug = models.SlugField(_('Slug'), max_length=255, unique=True)
    description = models.TextField(_('Description'), blank=True)
    photo = models.ImageField(_('Photo'), upload_to='artists_photos', blank=True,
                              null=True, max_length=255)

    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)

    # This field is used by Haystack to reindex search
    date_updated = models.DateTimeField(
        _("Date updated"), auto_now=True, db_index=True)


    class Meta:
        app_label = 'catalogue'
        ordering = ['-first_name']
        verbose_name = _('Artist')
        verbose_name_plural = _('Artists')

    def __str__(self):
        return u"%s %s " % (self.first_name, self.surname,)

    # def get_absolute_url(self):
        # return reverse('catalogue:detail',
        #                kwargs={'product_slug': self.slug, 'pk': self.id})

# __all__.append('Artist')